================================================
COVID-19 Symptom Detector from Social Media Text
================================================

|
| This is an implementation of the COVID-19 Symptom Detector from Social Media Text used in the JAMIA paper, `Self-reported COVID-19 symptoms on Twitter: an analysis and a research resource <https://academic.oup.com/jamia/article/27/8/1310/5867237>`_.
|

Requirement
-----------

|
| Python
| Python packages: pandas, fuzzywuzzy, python-levenshtein
|

Installation
------------

|
| Run command *python setup.py install*
|

How to use it
-------------
The package includes

- Loading a curated symptom lexicon of COVID-19 symptoms
- Performing a fuzzy search for those symptoms

A sample lexicon (accompanying the above paper) available at:
https://docs.google.com/document/d/1CdhpuNbCV4egYv0TA7hpTqVL3ZfAxyLQipAepDfPzBQ

|
| Script on bitbucket contains a sample lexicon from Twitter.
|

Details
=======

Inputs:

- tsv/csv file containing the lexicon (format: Standardized Term CUI Expression)
- tsv/csv file containing texts from which symptoms are to be extracted (format: any tsv; last column must contain text)

Outputs:

- a dataframe with columns for detected symptoms in the standard form, the symptom expressions in the text, and the text that symptom is detected.
- a summary of detect symptoms-a list of all detected symptoms in the standard form

Exmaples:
========
With *COVID-Twitter-Symptom-Lexicon.tsv* as lexicon and *sample.csv* as sample tweets (both in folder *data*), run the following command in Python

.. code-block:: python
   
    >>> from symptomdetector.SymptomDetector import SymptomDetection # import 
    >>> symptom_detection = SymptomDetection('./COVID-Twitter-Symptom-Lexicon.tsv') # initialize a SymptomDetection object with the lexicon
    >>> symptom_df, summary_of_symptoms = symptom_detection.symptom_search('./sample.csv') # perform symptom search on the sample tweets


.. code-block:: python
    
    >>> print(summary_of_symptoms)
    ['Chest pain', 'Fatigue', 'Pyrexia', 'Cough', 'Body ache & Pain', 'Dyspnea', 'Chest tightness']
    
    >>> print(symptom_df)
         StandardSymptom     SymptomExpression                                                Text
    0         Chest pain           chest pains   hey everyone, first off, a little background. ...  
    1         Chest pain            chest pain   hey everyone, first off, a little background. ...   
    2              Cough                 cough   hey everyone, first off, a little background. ...   
    3            Dyspnea   shortness of breath   hey everyone, first off, a little background. ...
    4            Dyspnea  shortness of breathe   hey everyone, first off, a little background. ...
    5    Chest tightness       chest tightness   hey everyone, first off, a little background. ...
    6            Fatigue               fatigue   hey everyone, first off, a little background. ...
    7            Pyrexia                 fever   hey everyone, first off, a little background. ...
    8   Body ache & Pain                 aches   hey everyone, first off, a little background. ...
    9   Body ache & Pain         ache all over   hey everyone, first off, a little background. ...
    10           Dyspnea                   sob   hey guys, if you checked my history, i've test...   
    11   Chest tightness       chest tightness   hey guys, if you checked my history, i've test...
    12           Fatigue               fatigue   hey guys, if you checked my history, i've test...
    13  Body ache & Pain                 aches   hey guys, if you checked my history, i've test...

|

CITATION
--------
|
| Sarker A, Lakamana S, Hogg-Bremer W, Xie A, Al-Garadi MA, Yang YC. Self-reported COVID-19 symptoms on Twitter: an analysis and a research resource. J Am Med Inform Assoc. 2020 Aug 1;27(8):1310-1315. doi: 10.1093/jamia/ocaa116. PMID: 32620975; PMCID: PMC7337747.
|