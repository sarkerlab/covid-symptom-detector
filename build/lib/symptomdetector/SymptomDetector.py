'''

COVID-19 Symptom Detector from Social Media Text

Outline:
 - Load a curated symptom lexicon of COVID-19 symptoms
 - Perform a fuzzy search for those symptoms


@author Abeed Sarker

CITATION:

Sarker A, Lakamana S, Hogg-Bremer W, Xie A, Al-Garadi MA, Yang YC. Self-reported COVID-19 symptoms on Twitter: an
analysis and a research resource. J Am Med Inform Assoc. 2020 Aug 1;27(8):1310-1315. doi: 10.1093/jamia/ocaa116.
PMID: 32620975; PMCID: PMC7337747.

Sample lexicon (accompanying the above paper) available at:
https://docs.google.com/document/d/1CdhpuNbCV4egYv0TA7hpTqVL3ZfAxyLQipAepDfPzBQ

Script on bitbucket contains a sample lexicon from Twitter.

Inputs:
 - tsv/csv file containing the lexicon (format: Standardized Term\tCUI\tExpression
 - tsv/csv file containing texts from which symptoms are to be extracted (format: any tsv; last column must contain text)

'''

import pandas as pd
from collections import defaultdict
import re
from itertools import islice
from fuzzywuzzy import fuzz

class SymptomDetection():
    def __init__(self, PATH_TO_LEXICON='../data/COVID-Twitter-Symptom-Lexicon.tsv'):
        """
        Initilizing the class with lexicon.
        :param PATH_TO_LEXICON: the path to the tsv file for lexicon. The default lexicon is './COVID-Twitter-Symptom-Lexicon.tsv'
        """
        self.symptom_lexicon = defaultdict(list)
        lexicon_df = pd.read_csv(PATH_TO_LEXICON, sep='\t', encoding='utf8')
        for index, row in lexicon_df.iterrows():
            self.symptom_lexicon[row[0]].append(row[-1])
        print ('Symptom lexicon loaded, running search now')


    def slide_window(self, words, window_size):
        """
            This function creates the sliding window to search through the texts
            :param words: the list of words to be search through
            :param window_size: the size of the search window
            :return window: the tuple of the words in the window. It is an iterable
        """
        it = iter(words)
        window = tuple(islice(it, window_size))
        yield window
        for w in it:
            window = window[1:] + (w,)
            yield window

    def compute_similarity(self, text, symptom, max_threshold):
        """
        This function determines if the similarity between the text and a given symptom is above the give threshold.
        The similarity between the text and the symptom is defined as the maximum among all similarity scores between a
        window in the text and the symptom.
        :param text: the text to be search through
        :param symptom: the symptom to be matched
        :param max_threshold: the desired max_threshold
        :return: True if the similarity between the text and the symptom is above the threshold
        """
        # calculate the window size
        window_size = len(symptom.split())
        # adjust the threshold according to the window_size
        threshold = max_threshold - 2 * window_size
        for window in self.slide_window(text.split(), window_size):
            window_joined = ' '.join(window)
            # similarity score: a number between 0 and 100
            similarity_score = fuzz.ratio(window_joined, symptom)
            if similarity_score >= threshold:
                return True
        return False

    def symptom_search(self, PATH_TO_TEXT='../data/sample.csv', max_threshold=98):
        """
        This function search through the text according to the symtom in the symptom_lexicon
        :param PATH_TO_TEXT: the path to the csv file, for the text to be searched through
        :param max_threshold: the max_threshold parameter for compute_similarity
        :return symptom_df: a DataFrame contain filed, 'StandardSymptom', 'SymptomExpression', 'Text',
                             as the record of the search
        :return summary_of_symptoms: a list of unique StandardSymptom detected in the text
        """
        # Search texts for symptoms
        posts_df = pd.read_csv(PATH_TO_TEXT, encoding='utf-8')

        all_detected_symptoms = []
        for index, row in posts_df.iterrows():
            text = str.lower(row[-1])
            for k in self.symptom_lexicon.keys():
                v = self.symptom_lexicon[k]
                for symptom in v:
                    if re.search(r'\b'+symptom+r'\b',text) or self.compute_similarity(text,symptom,max_threshold):
                        all_detected_symptoms.append([k, symptom, text])


        symptom_df = pd.DataFrame(all_detected_symptoms, columns=['StandardSymptom', 'SymptomExpression', 'Text'])
        summary_of_symptoms = []
        for index, row in symptom_df.iterrows():
            summary_of_symptoms.append(row[0])

        summary_of_symptoms = list(set(summary_of_symptoms))
        print ('Symptoms discovered in text:')
        for s in summary_of_symptoms:
            print(s)

        return symptom_df, summary_of_symptoms

if __name__ == '__main__':
    symptom_detection = SymptomDetection()
    symptom_df, summary_of_symptoms = symptom_detection.symptom_search()
    posts_df = pd.read_csv('../data/sample.csv', encoding='utf-8')
    print(posts_df)
    print(symptom_df)
    print(summary_of_symptoms)