from setuptools import setup


def readme():
    with open('README.rst') as f:
        return f.read()


setup(name='symptomdetector',
      version='0.1',
      description='COVID-19 Symptom Detector from Social Media Text',
      long_description=readme(),
      classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.7.9',
      ],
      keywords='COVID, Symptom',
      url='https://bitbucket.org/sarkerlab/covid-symptom-detector/src/master/',
      author='Abeed Sarker',
      author_email='abeed@dbmi.emory.edu',
      license='MIT',
      packages=['symptomdetector'],
      install_requires=[
          'pandas',
          'fuzzywuzzy',
          'python-levenshtein'
      ],
      include_package_data=True,
      zip_safe=False)